import { Component } from '@angular/core';
import {Paho} from '../../node_modules/ng2-mqtt/mqttws31';
import { GaugeModule, GaugeSegment, GaugeLabel } from 'ng2-kw-gauge';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  temps: any[] = [];
  hums: any[] = [];
  tempTopic = "ROZNA/rpi1/temp";
  humTopic = "ROZNA/rpi1/hum";
  tempSegments: GaugeSegment[] = [];
  humSegments: GaugeSegment[] = [];
  tempLabels: GaugeLabel[] = [];
  humLabels: GaugeLabel[] = [];
  tempSeg: GaugeSegment;
  tempLbl: GaugeLabel;
  humSeg: GaugeSegment;
  humLbl: GaugeLabel;
  private _client: Paho.MQTT.Client;

  public constructor() {
    this._client = new Paho.MQTT.Client("vitez.si", 9001, "132456");
    //Temp
    this.tempSeg = new GaugeSegment();
    this.tempLbl = new GaugeLabel();
    this.tempLbl.text = "-";
    this.tempLbl.color = "#FFFFFF";
    this.tempLbl.x = 0;
    this.tempLbl.y = 0;
    this.tempSeg.borderWidth = 10;
    this.tempSeg.bgColor = "#00000000"
    this.tempSeg.value = 0;
    this.tempSeg.color = "#2196F3";
    this.tempSeg.goal = 100;
    this.tempSegments.push(this.tempSeg);
    this.tempLabels.push(this.tempLbl);
    //Hum
    this.humSeg = new GaugeSegment();
    this.humLbl = new GaugeLabel();
    this.humLbl.text = "-";
    this.humLbl.color = "#FFFFFF";
    this.humLbl.x = 0;
    this.humLbl.y = 0;
    this.humSeg.borderWidth = 10;
    this.humSeg.bgColor = "#00000000"
    this.humSeg.value = 0;
    this.humSeg.color = "#F44336";
    this.humSeg.goal = 100;
    this.humSegments.push(this.humSeg);
    this.humLabels.push(this.humLbl);
    this._client.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost.');
      console.log();
    };

    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      console.log('Message arrived.');
      console.log(message.payloadString);
      console.log(message.destinationName);
      if(message.destinationName == this.tempTopic)
      {
        this.temps.unshift(Number(message.payloadString));
        if(this.temps.length > 10)
        {
          this.temps.pop();
        }
        this.tempSeg.value = Number(message.payloadString);
        this.tempLbl.text = "Temperatura: "+message.payloadString+"°C";      
      }
      else if(message.destinationName == this.humTopic)
      {
        this.hums.unshift(Number(message.payloadString));
        if(this.hums.length > 10)
        {
          this.hums.pop();
        }
        this.humSeg.value = Number(message.payloadString);
        this.humLbl.text = "Vlaga: "+message.payloadString+"%RH";      
      }
      else
      {
        console.log("Unknown topic: "+message.destinationName);
      }
    };

    this._client.connect({ onSuccess: this.onConnected.bind(this) });
  }

  private onConnected():void {
    console.log('Connected to broker.');
    this._client.subscribe(this.tempTopic, { onSuccess: this.onSubscribed.bind(this) });
    this._client.subscribe(this.humTopic, { onSuccess: this.onSubscribed.bind(this) });
  }
  private onSubscribed():void {
    console.log("Subscribed...")
  }
}
